#!/bin/sh

#[PHP CS Fixer] validate added file

REDBG='\e[1;41m'
REDTEXT='\e[31m'
GREENBG='\e[1;42m'
GREENTEXT='\e[92m'
YELLOWBG='\e[1;45m'
YELLOWTEXT='\e[95;40m'
HEADERBG='\e[44m'
ENDSTYLE='\e[0;40m'
HASH=`git rev-parse --short HEAD`
FILES=`git diff-tree --no-commit-id --name-only -r ${HASH}`

check_code () {
   if [[ "$1" == *".php"* ]]
    then
        vendor/bin/php-cs-fixer fix $1 --dry-run --quiet
        if [ $? -ne 0 ]
          then
              printf "\t${REDBG} FAILED ${ENDSTYLE}${REDTEXT} $1 ${ENDSTYLE}\n"
              exit_code=1
          else
              printf "\t${GREENBG} PASSED ${ENDSTYLE}${GREENTEXT} $1  ${ENDSTYLE}\n"
        fi
    fi
    return $exit_code
}

start (){
  local exit_code=0
  printf "\n\n${HEADERBG}========== Running PHP-CS-Fixer ==========${ENDSTYLE}\n\n"

  # git diff --cached --name-only | while read line;
  git diff-tree --no-commit-id --name-only -r ${HASH} | while read line;
    do
      printf "$line"
       check_code "$line"
    done

  exit_code=$?

  if [[ $exit_code -ne 0 ]];
    then
      printf "\nPlease fix ${REDBG} FAILED ${ENDSTYLE} items before committing. ${ENDSTYLE}\n"
      printf "${YELLOWTEXT}To fix, run: ${ENDSTYLE}${YELLOWBG}vendor/bin/php-cs-fixer fix [filename]${ENDSTYLE}\n"
    else
      printf "\n ${GREENBG}No issues found.${ENDSTYLE}\n"
  fi

  printf "${HEADERBG}==========================================${ENDSTYLE}\n"
  return $exit_code
}

git branch

display_start=$(start)
return=$?

printf "\n${display_start}\n"

exit $return